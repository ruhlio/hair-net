(function() {
  stylist.initSchedule = function() {

    var calendar = $('#calendar');
    var selectedService = $('#selectedService');
    var selectedClient = $('#selectedClient');

    _.each( stylist.services, function( service ) {
      selectedService.append( '<option value="' + service.id + '">' + service.name + '</option>' );
    });

    $.get( 'data/clients.json?' + (new Date().getTime()), function( clientsString ) {
      var clients = clientsString.clients;

      _.each( clients, function( client ) {
        var clientName = app.grabProfileValue( client.personal_info, "Name" );
        selectedClient.append( '<option value="' + clientName + '">' + clientName + '</option>' );
      });
    });

    calendar.fullCalendar({
      editable: true,
      defaultView: 'agendaWeek',

      dayClick: function( day, allDay, jsEvent, view ) {
        var serviceId = selectedService.val();

        if ( serviceId ) {
          var service = _.find( stylist.services, function( service ) {
            return service.id == serviceId;
          });

          calendar.fullCalendar('renderEvent', {
            id: service.id,
            title: service.name + " with " + selectedClient.val(),
            allDay: false,
            start: day,
            end: moment(day).add('minutes', service.duration).toDate()
          });
        }
        else {
          app.alert('Please select a service');
        }
      },

      eventClick: function( event, jsEvent, view ) {
        calendar.fullCalendar('removeEvents', event.id );
      }

    });

  };

})();
