(function() {

  String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
  };

  function addAlert( message, type ) {
    var alert = $('<div class="alert"></div>')
      .append('<button type="button" class="close" data-dismiss="alert">&times;</button>')
      .append( message );

    if ( type ) {
     alert.addClass( type );
    }

    $('#alerts').append( alert );
    setTimeout( function() {
      alert.fadeOut( 2000, function() {
        alert.remove();
      } );
    }, 4000 );
  }

  function _addElementLoadingData( element, templateName, dataName, extractData ) {
    var time = new Date().getTime();

    if ( 0 === element.length ) {
      console.error( 'Template root does not exist' );
      return;
    }

    $.when( $.get( '_' + templateName + '.html?' + time),
            $.get('data/' + dataName + '.json?' + time) )
    .done( function( profileResult, stylistsResult ) {
      if ( "success" !== profileResult[1] ) {
       console.error("Failed to retrieve template '" + templateName + "'.\n" +
                         "Error: " + dataName);
         }
         else if ( "success" !== profileResult[1] ) {
           console.error("Failed to data '" + dataName + "'.\n" +
                         "Error: " + dataName);
         }

         var rawData = stylistsResult[0];
         var data = rawData;
         if ( extractData ) {
          data = extractData( rawData );
         }

         element.append(
           _.template( profileResult[0], {
             profile: data
           })
         );

      });
  }

  function _addElementWithData( element, templateName, data ) {
      var time = new Date().getTime();

      if ( 0 === element.length ) {
        console.error( 'Template root does not exist' );
        console.log( element );
        return;
      }

      $.get( '_' + templateName + '.html?' + time, function( templateHtml ) {
          element.append(
            _.template( templateHtml, data )
          );

      });
  }

  var app = {
    error: function( message ) {
      addAlert( message, 'alert-error' );
    },

    alert: function( message ) {
      addAlert( message );
    },

    success: function( message ) {
      addAlert( message, 'alert-success' );
    },

    logout: function() {
      window.location = 'index.html';
    },

    grabProfileValue: function( values, valueName ) {
      return _.find( values, function( value ) {
        return valueName === value.name;
      }).value;
    },

    loadTemplate: function ( element, templateName, dataOrDataName, extractData ) {
      if ( 'string' === typeof dataOrDataName ) {
        _addElementLoadingData( element, templateName, dataOrDataName, extractData );
      }
      else {
        _addElementWithData( element, templateName, dataOrDataName, extractData );
      }
    },

    renderTemplate: function( templateName, dataOrDataName, extractData ) {
      var element = $(document.createElement('div'));

      if ( 'string' === typeof dataOrDataName ) {
        _addElementLoadingData( element, templateName, dataOrDataName, extractData );
      }
      else {
        _addElementWithData( element, templateName, dataOrDataName, extractData );
      }

      return element.html();
    },

    createRouting: function( namespace ) {
      var content = $('#content');

      function loadPage( pageName ) {
        var namespacedPageName = namespace + '_' + pageName;

        $.get(namespacedPageName + '.html?' + new Date().getTime(), function( page ) {
          content.html( page );

          var initFunctionName = 'init' + _.map( pageName.split('_'), function( word, index ) {
            return word.capitalize();
          }).join('');

          var initPage = window[namespace][initFunctionName];
          if ( initPage ) {
            initPage();
          }
          else {
            console.warn( "Init method for page '" + pageName + "' not found.\n" +
                          "Searched for '" + namespace + '.' + initFunctionName + "'.");
          }

          $('.navigation li').removeClass('active');
          $('#' + pageName + '_nav').addClass('active');
        });
      }

      $('.navigation li[id$="_nav"]').click(function() {
        var pageParts = $(this).attr('id').split('_');
        var pageName = pageParts.slice( 0, pageParts.length - 1 ).join('_');
        loadPage( pageName );
      });

      return {
        loadPage: loadPage
      };
    }

  };
  window.app = app;

})();
