(function() {
  client.initSchedule = function() {

    var calendar = $('#calendar');
    var selectedService = $('#selectedService');
    var services = [];

    $.get('data/services.json', function( servicesString ) {
      services = servicesString.services;

      _.each( services, function( service ) {
        selectedService.append( '<option value="' + service.id + '">' + service.name + "</option>" );
      });
    });

    calendar.fullCalendar({
      editable: true,
      defaultView: 'agendaWeek',

      dayClick: function( day, allDay, jsEvent, view ) {
        var serviceId = selectedService.val();

        if ( serviceId ) {
          var service = _.find( services, function( service ) {
            return service.id == serviceId;
          });

          calendar.fullCalendar('renderEvent', {
            id: service.id,
            title: service.name,
            allDay: false,
            start: day,
            end: moment(day).add('minutes', service.duration).toDate()
          });
        }
        else {
          app.alert('Please select a service');
        }
      },

      eventClick: function( event, jsEvent, view ) {
        calendar.fullCalendar('removeEvents', event.id );
      }

    });

  };

})();
