window.stylist = {};

$(function() {
  $.get( 'data/services.json?' + (new Date().getTime()), function( servicesString ) {
    stylist.services = servicesString.services;

    stylist.routing = app.createRouting( 'stylist' );
    stylist.routing.loadPage('services');
  });

  $('#logout').click(function() {
    app.logout();
  });

});
