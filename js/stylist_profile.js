(function() {
  stylist.initProfile = function() {
    app.loadTemplate( $('#profile'), 'profile', 'stylists', function( rawData ) {
      return rawData.stylists[0];
    });
  };

})();
