(function() {
  stylist.initServices = function() {

    var servicesTable = $('#services');

    _.each( stylist.services, function( service ) {
      addService( service );
    });

    $('.add-service').click( createServiceDialogCallback( "Add a Service", "Add", {} ) );

    function createServicpareDialogCallback( header, action, service ) {
      return function() {
        $.get( '_editService.html?' + (new Date().getTime()), function( template ) {
          var modal = $(_.template( template, {
            header: header,
            service: service,
            action: action
          }));

          modal.find( '.modal-footer .btn-primary').click( function() {
            service = {
              id: modal.find('#serviceId').val() || _.uniqueId('service_'),
              name: modal.find('#serviceName').val(),
              description: modal.find('#serviceDescription').val(),
              duration: modal.find('#serviceDuration').val(),
              price: modal.find('#servicePrice').val()
            };

            addService( service );
            modal.modal('hide');
          });

          modal.modal('show');
        });
      };
    }

    function addService( service ) {
      var existingServiceCount = _.filter(stylist.services, function( otherService ) {
        return otherService.id == service.id;
      }).length;

      if ( !existingServiceCount ) {
        stylist.services.push( service );
      }

      $.get( '_service.html?' + (new Date().getTime()), function( template ) {
        var row = $(_.template( template, { service: service } ));

        row.find('.delete-service').click( deleteService );
        row.find('.edit-service').click( createServiceDialogCallback( "Edit Service", "Save", service ) );

        servicesTable.find( 'tr[data-id="' + service.id + '"]').remove();

        servicesTable.append( row );
      });
    }

    function deleteService() {
      var row = $(this).parents('tr');
      var id = row.data('id');

      stylist.services = _.reject( stylist.services, function( service ) {
        return service.id === id;
      });

      row.remove();
    }

  };

})();
