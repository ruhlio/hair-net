window.client = {};

$(function() {
  client.routing = app.createRouting('client');
  client.routing.loadPage('schedule');

  $('#logout').click(function() {
    app.logout();
  });

});
