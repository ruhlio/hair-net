(function() {
  client.initFindStylist = function() {

    var searchBox = $('#searchBox');
    var searchResults = $('#searchResults');

    $('#searchButton').click( function() {
      var searchValue = searchBox.val().toLowerCase();
      searchResults.empty();

      $.get( 'data/stylists.json?' + (new Date().getTime()), function( rawData ) {
        var data = rawData;

        var foundStylists = _.map( _.filter( data.stylists, function( stylist ) {
          var name = app.grabProfileValue( stylist.personal_info, "Name" );
          return (-1 !== name.toLowerCase().indexOf( searchValue ));
        }),

        function( stylistProfile ) {
          var name = app.grabProfileValue( stylistProfile.personal_info, "Name" );
          var location = app.grabProfileValue( stylistProfile.personal_info, "Location" );
          var salon = app.grabProfileValue( stylistProfile.personal_info, "Salon" );

          return {
            photo: stylistProfile.photo,
            name: name,
            location: location,
            salon: salon
          };
        });

        app.loadTemplate( $('#searchResults'), 'searchStylistResults', {
          stylists: foundStylists
        });

      });
    });

  };

})();
